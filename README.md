In this repo:
* `Dockerfile`: to build a filebeat Docker image suitable to be deployed
  on Openshift
* `filebeat.yml`: Filebeat configuration file
* `filebeat-openshift.yml`: deploymentConfig template consisting of an
  application container logging to a volume, and a filebeat container
  reading from the volume and sending as configured

Workflow:
* Build the docker image
```
$ docker build . -t gitlab-registry.cern.ch/mailservices/mailadminservice-next/filebeat-mas-next:latest
```
* Push the docker image to the registry
```
$ docker push gitlab-registry.cern.ch/mailservices/mailadminservice-next/filebeat-mas-next:latest
```
* Re-deploy the Openshift app
