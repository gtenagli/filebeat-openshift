FROM docker.elastic.co/beats/filebeat:7.1.0
USER root
RUN chmod -R a+r /usr/share/filebeat/
USER root
RUN find /usr/share/filebeat/ -executable -exec chmod a+x {} \;
USER filebeat
